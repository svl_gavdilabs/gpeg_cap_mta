const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("./server");
const { expect } = require("chai");

//For private function testing
const rewire = require("rewire");
const generator = rewire("../srv/generator");
const emailGenerator = generator.__get__('generateUniqueSignature'); 
const nameGenerator = generator.__get__('getFirstname');
const employeeGenerator = generator.__get__('GenerateEmployee');
const phoneGenerator = generator.__get__('generatePhonenumber');
const determiner = generator.__get__('chooseDirectory');

// Database files
const databaseNames = require('../srv/data/DK_Names.json'),
    dataMaleNames = databaseNames.male,
    dataFemaleNames = databaseNames.female,
    dataLastnames = databaseNames.lastname;
const baselineFile = require('../srv/data/DK_Baseline.json');

chai.use(chaiHttp);
chai.should();

let app = null;
let firstGenEmployees = [];

before((done) => {
    server.then((result) => {
        app = result;
        done();
    });
});

//Email Generation Testing
describe("Generate Unique Email Function", () =>{   
        it("+ Should generate 10000 unique signatures", () =>{        
        const firstname = 'John', lastname = 'Smith';
        let sigArray = [];
        let allUnique = true;        
        
        for(let i = 0; i < 10000; i++){
            let signature = emailGenerator(firstname, lastname);
            if(sigArray.includes(signature)){
                allUnique = false;
                break;
            }
            sigArray.push(signature);
        }
        expect(allUnique).to.be.equal(true);
    });
});

//First name generator testing
describe("Generate Firstname Based on Gender", () =>{
    const standardInfo = {};   
    const testMaleNames = ['Lobby', 'Nobby', 'Grubby'],
        testFemaleNames = ['Ibsy', 'Dipsy', 'Lala'],
        testInfo = {maleFirstnames: testMaleNames, femaleFirstnames: testFemaleNames};

    it("+ Should generate a male name from the database", () =>{
        const name = nameGenerator(true, standardInfo);
        expect(dataMaleNames).to.include(name);
    });
    it("+ Should generate a female name from the database", () =>{
         const name = nameGenerator(false, standardInfo);
         expect(dataFemaleNames).to.include(name);
    });
    it("+ Should generate a male name based on input", () =>{
        const name = nameGenerator(true, testInfo);
        expect(testMaleNames).to.include(name);
    });
    it("+ Should generate a female name based on input", () =>{
        const name = nameGenerator(false, testInfo);
        expect(testFemaleNames).to.include(name);
    });
});

//Phonenumber length testing
describe("Generating Phonenumber", () => {
    const baselineDigits = baselineFile.phoneDigits;

    it("+ Should generate a 8 digit phonenumber", () =>{
        let phoneNumber = phoneGenerator(baselineDigits);
        phoneNumber = phoneNumber.split('');
        expect(phoneNumber).to.have.lengthOf(baselineDigits);
    });
    it("+ Should generate a 12 digit phonenumber", () =>{
        const testDigits = 12;
        let phoneNumber = phoneGenerator(testDigits);
        phoneNumber = phoneNumber.split('');
        expect(phoneNumber).to.have.lengthOf(testDigits);
    });
    it("+ Should automatically choose to generate 8 digits", () =>{
        let phoneNumber = phoneGenerator(determiner(undefined, baselineDigits));
        phoneNumber = phoneNumber.split('');
        expect(phoneNumber).to.have.lengthOf(baselineDigits);
    });
});

//Generate Employee Function Test
describe("Generate Employee Function", async () =>{
    const information = {
        domain: "@gavdilabs.com",
        amount: "1",
        genPhone: true,
        genAddress: true,
        minAge: 18,
        maxAge: 60,
        femaleOnly: false,
        maleOnly: false
    }
    const employee = await employeeGenerator(information);

    it("+ Should generate an employee with a firstname", () =>{
        
        if(employee.male)
            expect(dataMaleNames).to.include(employee.firstName);
        else expect(dataFemaleNames).to.include(employee.firstName);
    });
    it("+ Should generate an employee with a lastname", () =>{
        expect(dataLastnames).to.include(employee.lastName);
    });
});

//API Call Tests
describe("Generate Employees Operation", () => {
    it("+ should generate 10 employees", (done)=>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 10,
                    genPhone: true,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false
                }
            })
            .end((error, response) => {
                try {
                    response.should.have.status(200);
                    response.body.value.should.be.an("array").to.have.lengthOf(10);
                    firstGenEmployees = response.body.value;
                    done();
                } catch (error){
                    done(error);
                }
            });
    });
    it("+ generated employees should be readable from EmployeeSet", (done) =>{
        chai.request(app)
            .get("/employee-generator/EmployeeSet")
            .end((error, response)=>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(10);
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate new and less employees than the prior call", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 2,
                    genPhone: true,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false
                }
            })
            .end((error, response) => {
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(2);
                    expect(response.body.value).to.not.equal(firstGenEmployees);
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate 10000 employees", (done)=>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 10000,
                    genPhone: true,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false
                }
            })
            .end((error, response) => {
                try {
                    response.should.have.status(200);
                    response.body.value.should.be.an("array").to.have.lengthOf(10000);
                    firstGenEmployees = response.body.value;
                    done();
                } catch (error){
                    done(error);
                }
            });
    });
    it("+ should generate female entries only", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: '@gavdilabs.com',
                    amount: 10,
                    genPhone: true,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: true,
                    maleOnly: false
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(10);

                    const data = response.body.value;
                    let isAllFemale = true;

                    data.forEach(element => {
                        if(element.male)
                            isAllFemale = false;
                    });

                    expect(isAllFemale).to.be.equal(true);
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate male entries only", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 10,
                    genPhone: true,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: true
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(10);

                    const data = response.body.value;
                    let isAllMale = true;

                    data.forEach(element => {
                        if(!element.male)
                            isAllMale = false;
                    });

                    expect(isAllMale).to.be.equal(true);
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should not generate addresses", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 10,
                    genPhone: true,
                    genAddress: false,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(10);

                    const data = response.body.value;
                    data.forEach(element => {
                        expect(element.streetname).to.be.equal('');
                        expect(element.streetnumber).to.be.equal(0);
                        expect(element.postalcode).to.be.equal('');
                    });
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should not generate phonenumber", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 10,
                    genPhone: false,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(10);

                    const data = response.body.value;
                    data.forEach(element => {
                        expect(element.cellPhone).to.be.equal('');
                    });
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate name based on male name given in request", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 1,
                    genPhone: false,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: true,
                    maleFirstnames:[
                        "Tester", "Funky", "Chunky"
                    ]
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(1);

                    const name = response.body.value[0].firstName;
                    const testMaleNames = ["Tester", "Funky", "Chunky"];
                    expect(testMaleNames).to.include(name);                    
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate name based on female name given in request", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 1,
                    genPhone: false,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: true,
                    maleOnly: false,
                    femaleFirstnames:[
                        "Ibsy", "Dipsy", "Lala"
                    ]
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(1);
                    
                    const name = response.body.value[0].firstName;
                    const testFemaleNames = ["Ibsy", "Dipsy", "Lala"];
                    expect(testFemaleNames).to.include(name);                    
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate lastname based on lastname given in request", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 1,
                    genPhone: false,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false,
                    lastnames:[
                        "Oogie", "Wookie", "Noomie"
                    ]
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(1);
                    
                    const lastname = response.body.value[0].lastName;
                    const testLastnames = ["Oogie", "Wookie", "Noomie"];
                    expect(testLastnames).to.include(lastname);                    
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate an employee with custom country and countrycode", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 1,
                    genPhone: false,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false,
                    country: "PT",
                    countrycode: "+351"
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(1);
                    
                    const country = response.body.value[0].country;
                    const countryCode = response.body.value[0].countryCode;
                    const testCountry = "PT";
                    const testCountryCode = '+351';
                    expect(testCountry).to.be.equal(country);
                    expect(testCountryCode).to.be.equal(countryCode);                  
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate an employee with a custom address", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 1,
                    genPhone: false,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false,
                    addresses:[
                        {
                            streetname: "Nowhere",
                            postalcode: "0000"
                        },
                        {
                            streetname: "Somewhere",
                            postalcode: "1111"
                        }
                    ]
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(1);
                    
                    const streetname = response.body.value[0].streetname;
                    const postalcode = response.body.value[0].postalcode;                    
                    const testAddresses = {
                        addresses:[
                            {
                                streetname: "Nowhere",
                                postalcode: "0000"
                            },
                            {
                                streetname: "Somewhere",
                                postalcode: "1111"
                            }
                        ]
                    }    
                    let foundPostal = false, foundStreet = false;
                    testAddresses.addresses.forEach(element => {
                        if(element.streetname == streetname)
                            foundStreet = true;
                        if(element.postalcode == postalcode)
                            foundPostal = true;
                    });

                    expect(foundPostal).to.be.equal(true);
                    expect(foundStreet).to.be.equal(true);
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
    it("+ should generate an employee with a custom address", (done) =>{
        chai.request(app)
            .post("/employee-generator/GenerateEmployees")
            .set('content-type', 'application/json')
            .send({
                info:{
                    domain: "@gavdilabs.com",
                    amount: 1,
                    genPhone: true,
                    genAddress: true,
                    minAge: 18,
                    maxAge: 60,
                    femaleOnly: false,
                    maleOnly: false,
                    phoneDigits: 11
                }
            })
            .end((error, response) =>{
                try{
                    expect(response).to.have.status(200);
                    expect(response.body.value).to.be.an("array").to.have.lengthOf(1);
                    const digits = response.body.value[0].cellPhone.toString().split('').length;
                    expect(digits).to.be.equal(11)
                    done();
                } catch(error){
                    done(error);
                }
            });
    });
});

const calculator = require('../srv/calculator.js');

describe("Calculation Actions and Methods", ()=>{
    it("+ Should return 33.33 for male and 66.66 for female in float value", async () =>{
        const oneMaleTwoFemale = [
            {male: true},
            {male: false},
            {male: false}
        ];

        const result = calculator.CalcGenderDist(oneMaleTwoFemale),
            expectedMale = '33.33',
            expectedFemale = '66.66',
            parsedMale = result.male.toString().slice(0, 5),
            parsedFemale = result.female.toString().slice(0, 5);
        expect(parsedMale).to.equal(expectedMale);
        expect(parsedFemale).to.equal(expectedFemale);
    });
});
