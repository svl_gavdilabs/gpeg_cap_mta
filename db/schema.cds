namespace gp.service;

entity Employee {
    key email : String;
    male : Boolean;
    firstName : String;
    lastName : String;
    dateOfBirth : String;
    streetname : String;
    streetnumber : Integer;
    postalcode : String;
    cellPhone : String;
    country : String;
    countryCode : String;
    pictureUrl : String;
}

type GenerationData{
    domain: String;
    amount: Integer;
    genPhone: Boolean;
    genAddress: Boolean;
    minAge: Integer;
    maxAge: Integer;
    femaleOnly: Boolean;
    maleOnly: Boolean;

    //OPTIONAL PARAMETERS
    maleFirstnames: many String;
    femaleFirstnames: many String;
    lastnames: many String;
    country: String;
    countrycode : String;
    addresses : many Addresses;
    phoneDigits : Integer;
}

type Addresses{
    streetname: String;
    postalcode: String;
}

type GenderCount{
    male: DecimalFloat;
    female: DecimalFloat;
}