sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	'sap/m/MessageToast',
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/FilterType",
	"sap/ui/export/library",
	"sap/ui/export/Spreadsheet"
], function (BaseController, JSONModel, formatter, MessageToast, Filter, FilterOperator, FilterType, exportLibrary, Spreadsheet) {
	"use strict";

	jQuery.sap.require("jquery.sap.storage");

	return BaseController.extend("com.gavdilabs.gp-frontend.controller.Worklist", {

		formatter: formatter,

		onInit : function () {
			var oViewModel;
			this._largeGenButton = this.byId("largeGenButton");
			this._updateGenText();
			this._oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			this._loadUserPreference();
		},

		//On clicking object in table
		onPress : function (oEvent) {
			this._showObject(oEvent.getSource());
		},

		onNavBack : function() {
			history.go(-1);
		},

		onRefresh : function () {
			var oTable = this.byId("table");
			oTable.getBinding("items").refresh();
			this._updateGenText();
		},

		onDataPress : function(){
			this.getRouter().navTo("data");			
		},

		onGenerate : function() {
			var iAmount = parseInt(this.byId("inputNumber").getValue()),
				oAgeRange = this.byId("ageRangeSlider");
				

			if(Number.isNaN(iAmount) || iAmount == 0){
				MessageToast.show("Invalid Generation Amount");
				return;
			}

			var oInfo = {
				info:{
					domain: this._sCustomDomain == undefined ? '@gavdilabs.com' : this._sCustomDomain,
					amount: iAmount,
					genPhone: this.byId("phoneCheckbox").getSelected(),
					genAddress: this.byId("addressCheckbox").getSelected(),
					minAge: oAgeRange.getValue(),
					maxAge: oAgeRange.getValue2(),
					femaleOnly: this.byId("femaleCheckbox").getSelected(),
					maleOnly: this.byId("maleCheckbox").getSelected(),
					maleFirstnames: this._oCustomMale,
					femaleFirstnames: this._oCustomFemale,
					lastnames: this._oCustomLastnames,
					country: this._sCustomCountry,
					countrycode: this._sCustomCountryCode,
					addresses: this._oCustomAddresses == undefined ? undefined : this._oCustomAddresses.addresses,
					phoneDigits: this._iCustomDigits
				}
			}

			this._largeGenButton.setEnabled(false);
			var oTable = this.byId("table");
			oTable.setBusy(true);

			var oController = this;
			var oModel = this.getView().getModel();
			var oOperation = oModel.bindContext('/GenerateEmployees(...)');
			oOperation.setParameter('info', oInfo.info);
			oOperation.execute().then(
				async function(res){
					await res;
					MessageToast.show(iAmount + ' employees generated');
					oTable.setBusy(false);
					oController.onRefresh();
					oController._largeGenButton.setEnabled(true);
				}
			);
		},

		onOpenSettings : function(){
			if(!this._oSettingsDialog){
				this._oSettingsDialog = sap.ui.xmlfragment(this.getView().getId(), "com.gavdilabs.gp-frontend.view.fragments.SettingsDialog", this);
			}
			this._oSettingsDialog.open();
			this.byId("darkToggle").setState(this._oPref.darkToggleState);
		},

		onDarkToggle : function(oEvent){
			var oToggle = oEvent.getSource(),
				bState = oToggle.getState();
			sap.ui.getCore().applyTheme(bState ? "sap_fiori_3_dark" : "sap_fiori_3");
			this._saveUserPreference();
		},

		onSaveSettings : function(){
			this._extractToArray('customMaleInput', '_oCustomMale');
			this._extractToArray('customFemaleInput', '_oCustomFemale');
			this._extractToArray('customLastnamesInput', '_oCustomLastnames');
			this._extractToJson('customAddressesInput', '_oCustomAddresses');

			this._sCustomDomain = this._extractInputValue('customDomainInput');
			this._sCustomCountry = this._extractInputValue('customCountryInput');
			this._sCustomCountryCode = this._extractInputValue('customCountryCodeInput');
			this._iCustomDigits = this._extractInputValue('customPhoneDigitsInput') != undefined ? 
				parseInt(this._extractInputValue('customPhoneDigitsInput')) : undefined;
			
			this._oSettingsDialog.close();
		},

		onSettingsClose : function(){
			this._oSettingsDialog.close();
		},

		onResetSettings : function(){
			this._oCustomMale = this._resetCustomSetting('customMaleInput');
			this._oCustomFemale = this._resetCustomSetting('customFemaleInput');
			this._oCustomLastnames = this._resetCustomSetting('customLastnamesInput');
			this._oCustomAddresses = this._resetCustomSetting('customAddressesInput');
			this._sCustomDomain = this._resetCustomSetting('customDomainInput');
			this._sCustomCountry = this._resetCustomSetting('customCountryInput');
			this._sCustomCountryCode = this._resetCustomSetting('customCountryCodeInput');
			this._iCustomDigits = this._resetCustomSetting('customPhoneDigitsInput');
		},

		onPressDownload : function(){
			this._exportSheet();
		},

		/******** INTERNAL METHODS **********/

		//Navigate to object page
		_showObject : function (oItem) {
			var that = this;
			oItem.getBindingContext().requestCanonicalPath().then(function (sObjectPath) {
				that.getRouter().navTo("object", {
					objectId_Old: oItem.getBindingContext().getProperty("email"),
					objectId : sObjectPath.slice("/EmployeeSet".length) // /Products(3)->(3)
				});
			});
		},

		_extractToArray : function(sId, sVariableName){
			var sValue = this._extractInputValue(sId),
				aValues = formatter.stringToArray(sValue);
			this[sVariableName] = aValues;
		},

		_extractToJson : function(sId, sVariableName){
			var sValue = this._extractInputValue(sId),
				oJSON = formatter.textToJSON(sValue);
			this[sVariableName] = oJSON !== '' ? oJSON : undefined;
		},

		_resetCustomSetting : function(sId){
			var oComponent = this.byId(sId);
			oComponent.setValue('');
			return undefined;
		},

		_extractInputValue : function(sId){
			var oComponent = this.byId(sId),
				sValue = oComponent.getValue();
			return sValue === '' ? undefined : sValue;
		},

		_getEmployeeAmount : function(){
			return $.get({
				url: "/employee-generator/EmployeeSet/$count",
				success: function(data){
					return data;
				},
				error: function(err){
					console.log('failed with error:' )
					console.log(err);
				}
			});
		},

		_updateGenText: async function(){
			var oGenText = this.byId('genLinkText');
			oGenText.setText('Total Generated Employees: ' + await this._getEmployeeAmount());
		},

		_loadUserPreference : async function(){
			this._oPref = this._oStorage.get("userPref");
			if(this._oPref){
				sap.ui.getCore().applyTheme(this._oPref.theme);
			}
		},

		_saveUserPreference : function (){
			var oToggle = this.byId("darkToggle"),
				bState = oToggle.getState();

			var oNewPref = {
				theme: sap.ui.getCore().getConfiguration().getTheme(),
				darkToggleState : bState
			};
			this._oStorage.put("userPref", oNewPref);
		},

		_exportSheet : async function(){
			var aCols, oTableItems, oRows, oSettings, oSheet, oTable;
			
			oTable = this.byId("table");
			oTableItems = oTable.getItems();
			oRows = oTable.getBinding('items');
			aCols = this._createColumnConfig();

			var aFilters = [];

			oTableItems.forEach(function(element){
				if(!element.getSelected()){
					var sEmail = element.getBindingContext().getProperty('email');
					aFilters.push(new Filter("email", FilterOperator.NotContains, sEmail));
				}
			});

			if(aFilters.length > 0)
				oRows.filter(new Filter({
					filters: aFilters,
					and: true
			}));

			var oModel = oRows.getModel();
			var iEmployeeAmount = parseInt(await this._getEmployeeAmount());

			oSettings = {
				workbook:{
					columns: aCols,
					heirachyLevel: 'Level'
				},
				dataSource: {
					type: 'OData',
					dataUrl: oRows.getDownloadUrl ? oRows.getDownloadUrl() : null,
					service: '/employee-generator',
					headers: oModel.getHeaders ? oModel.getHeaders() : null,
					count : iEmployeeAmount,//oRows.getLength ? oRows.getLength() : null,
					useBatch: true
				},
				fileName: 'generated_employees',
				worker: true
			}; 

			oSheet = new Spreadsheet(oSettings);
			oSheet.build().finally(function(){
				oSheet.destroy();
				oRows.filter([]);
			});
			
			this.onRefresh();
		},

		_createColumnConfig : function(){
			var EdmType = exportLibrary.EdmType;
			var aCols = [];

			aCols.push({
				label: 'email',
				property: 'email',
				type: EdmType.String
			});

			aCols.push({
				label: 'male',
				property: 'male',
				type: EdmType.Boolean
			});

			aCols.push({
				label: 'firstName',
				property: 'firstName',
				type: EdmType.String
			});

			aCols.push({
				label: 'lastName',
				property: 'lastName',
				type: EdmType.String
			});

			aCols.push({
				label: 'dateOfBirth',
				property: 'dateOfBirth',
				type: EdmType.String
			});

			aCols.push({
				label: 'streetname',
				property: 'streetname',
				type: EdmType.String
			});

			aCols.push({
				label: 'streetnumber',
				property: 'streetnumber',
				type: EdmType.Number
			});

			aCols.push({
				label: 'postalcode',
				property: 'postalcode',
				type: EdmType.String
			});

			aCols.push({
				label: 'cellPhone',
				property: 'cellPhone',
				type: EdmType.String
			});

			aCols.push({
				label: 'country',
				property: 'country',
				type: EdmType.String
			});

			aCols.push({
				label: 'countryCode',
				property: 'countryCode',
				type: EdmType.String
			});

			return aCols;
		}

	});
});