sap.ui.define([
	"./BaseController",
	"sap/ui/core/routing/History",
	"../model/formatter"
], function (BaseController, History, formatter) {
	"use strict";

	return BaseController.extend("com.gavdilabs.gp-frontend.controller.DataViz", {

        formatter: formatter,
        
		onInit : async function () {
			sap.ui.core.UIComponent.getRouterFor(this).getRoute("data").attachMatched(this.onEnter, this);
		},

		onEnter : async function(){
			this._setupGenderDonutChart();
		},
        
		onNavBack : function() {
			var sPreviousHash = History.getInstance().getPreviousHash();
			if (sPreviousHash !== undefined) {
				// eslint-disable-next-line sap-no-history-manipulation
				history.go(-1);
			} else {
				this.getRouter().navTo("worklist", {}, true);
			}
		},

		_setupGenderDonutChart : async function(){
			var oMaleSegment = this.byId("maleSegment"),
				oFemaleSegment = this.byId("femaleSegment");
			
			var oCount = await $.post({
				url:"/employee-generator/GenderCount",
				contentType: 'application/json',
				success: function(data){
					return data;
				},
				error: function(err){
					console.log('failed with error: ');
					console.log(err);
				}
			});

			oMaleSegment.setValue(oCount.male);
			oMaleSegment.setDisplayedValue(oCount.male + '%');

			oFemaleSegment.setValue(oCount.female);
			oFemaleSegment.setDisplayedValue(oCount.female + '%');
		}
	});
});