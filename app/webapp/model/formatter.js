sap.ui.define([] , function () {
	"use strict";

	return {

		/**
		 * Rounds the number unit value to 2 digits
		 * @public
		 * @param {string} sValue the number string to be rounded
		 * @returns {string} sValue with 2 digits rounded
		 */
		numberUnit : function (sValue) {
			console.log(sValue);
			if (!sValue) {
				return "";
			}
			return parseFloat(sValue).toFixed(2);
		},

		determineGender : function(bMale){
			return bMale === "Yes" ? "Male" : "Female";
		},

		formatAge : function(sDate){
			var oDate = new Date(sDate),
				oToday = new Date(),
				iAge = oToday.getFullYear() - oDate.getFullYear(),
				oMonth = oToday.getMonth() - oDate.getMonth(); 

			if (oMonth < 0 || (oMonth === 0 && oToday.getDate() < oDate.getDate())) {
				iAge--;
			}

			return iAge;
		},

		formatStreetnumber : function(iNumber){
			return iNumber == 0 ? '-' : iNumber + ', ';
		},

		formatPhonenumber : function(sPhonenumber){
			return sPhonenumber === '' ? '-' : sPhonenumber;
		},

		textToJSON : function(sText){
			var jsonObject;
			try{
				jsonObject = JSON.parse(sText);
			} catch(err){
				return '';
			}
			return jsonObject;
		},

		stringToArray : function(sValue){
			if(sValue == undefined) return sValue;
			return sValue === '' ? undefined : sValue.split(', ');
		},

		formatDate : function(sValue){
			var sYear = sValue.slice(0, 4),
				sMonth = sValue.slice(5, 7),
				sDay = sValue.slice(8, 10);
			return [sDay, sMonth, sYear].join('-');
		}

	};

});