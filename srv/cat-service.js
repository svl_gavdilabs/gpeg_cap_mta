const cds = require ('@sap/cds');
const gen = require('./generator.js');
const calc = require('./calculator.js');

let falseInput = false;


module.exports = async function (srv) {
    const {EmployeeSet} = srv.entities();   
    let cachedEmployees = await cds.run(SELECT.from(EmployeeSet)); 
    
    srv.before('GenerateEmployees', async (req) => {
        const amount = req.data.info.amount;
        if(amount <= 0){
            falseInput = true;
            return;
        } else falseInput = false;
        await cds.run( DELETE.from(EmployeeSet));
    });

    srv.on('GenerateEmployees', async (req) => {
        if(falseInput) return cachedEmployees;
        return await gen.GenerateEmployees(req.data.info);
    });

    srv.after('GenerateEmployees', async (req) => {
        const data = req;
        if(falseInput) return;
        cachedEmployees = data;
        await cds.run(INSERT.into(EmployeeSet).entries(data));
    });

    srv.on('GenderCount', async (req) =>{
        if(cachedEmployees.length == 0) return {male: 0, female: 0};
        return calc.CalcGenderDist(cachedEmployees);
    });
}