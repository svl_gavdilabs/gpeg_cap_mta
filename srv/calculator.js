
module.exports = {
    CalcGenderDist : function(employees){
        return {
            male: CalcGenderPercent(employees, true), 
            female: CalcGenderPercent(employees, false)
        }
    }
}

const CalcGenderPercent = (employees, isMale) =>{
    let amount = 0;
    employees.forEach((element) =>{
        if(element.male == isMale)
            amount++;
    });
    return CalcPercent(amount, employees.length);
}; 

const CalcPercent = (amount, total) => amount / total * 100;