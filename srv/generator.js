const dataDir = './data/';
const baseline = require('./data/DK_Baseline.json');
const addresses = require(dataDir + baseline.addresses);
const names = require(dataDir + baseline.names);
const images = require(dataDir + baseline.images);
let emailArray = [];

module.exports = {
    GenerateEmployees: async function(info) {
        const amount = info.amount;
        let employees = [];
        emailArray = [];
        for(let i = 0; i < amount; i++){
            employees.push(await GenerateEmployee(info));
        }
        return employees;
    }
}

const GenerateEmployee = async(info) =>{
    const isMale = await determineGender(info.maleOnly, info.femaleOnly);
    const firstname = await getFirstname(isMale, info);
    const lastname = await getRandomFromArray(chooseDirectory(info.lastnames, names.lastname));
    const address = info.genAddress ? getRandomFromArray(chooseDirectory(info.addresses, addresses.adresses)) : 
        {streetname: '', postalcode:''};
    const ageRange = await getDateRangeOnAge(info.minAge, info.maxAge),
        randomDate = await generateRandomDate(ageRange.start, ageRange.end);

    return {
        email: await generateUniqueSignature(firstname, lastname) + info.domain,
        male: isMale,
        firstName: firstname,
        lastName: lastname,
        dateOfBirth: await convertDateToEDM(randomDate),
        streetname: address.streetname,
        streetnumber: info.genAddress ? Math.floor(Math.random() * 100) + 1 : 0,
        postalcode: address.postalcode,
        cellPhone: info.genPhone ? generatePhonenumber(chooseDirectory(info.phoneDigits, baseline.phoneDigits)) : '',
        country: chooseDirectory(info.country ,baseline.country),
        countryCode: chooseDirectory(info.countrycode, baseline.countrycode),
        pictureUrl: dataDir + (isMale ? getRandomFromArray(images.male) : getRandomFromArray(images.female))
    }
}

const getRandomFromArray = (array) => {
    const randomNum = Math.floor(Math.random() * array.length);
    return array[randomNum];
}

const shuffleArray = (array) =>{
    let currentIndex = array.length, temporaryValue, randomIndex;

    while( 0 !== currentIndex){
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

const determineGender = async (male, female) =>{
    if(male && !female)
        return true;
    if(female && !male)
        return false;
    return Math.random() < 0.5;
}

const chooseDirectory = (info, data) => {
    return info === undefined ? data : info;
}

const getFirstname = (male, info) => {
    const maleNames = info.maleFirstnames;
    const femaleNames = info.femaleFirstnames;
    return getRandomFromArray(male ? chooseDirectory(maleNames, names.male) : chooseDirectory(femaleNames, names.female));
}

const generateRandomDate = async (start, end) =>{
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
} 

const generatePhonenumber = (digits) =>{
    return Math.random().toString().slice(2, digits + 2);
}

const getDateRangeOnAge = async (min, max) => {
    const currentDate = new Date(),
          currentYear = currentDate.getFullYear();
    return {
        start: new Date(currentYear - max, 0 , 01), 
        end: new Date(currentYear - min, currentDate.getMonth(), currentDate.getDate())
    }
}

const convertDateToEDM = async (dateObject) => {
    const verifyDigits = (value) => {
        const verify = value < 10 ? '0' + value : value;
        return verify === '00' ? '01' : verify;
    }
    const date = dateObject.getFullYear() + '-' + verifyDigits(dateObject.getMonth()) + 
        '-' + verifyDigits(dateObject.getDate());

    const seconds = verifyDigits(dateObject.getSeconds());
    if(seconds.toString().length > 2) seconds.toString().slice(0, 3);

    const time = verifyDigits(dateObject.getHours()) + ':' + 
        verifyDigits(dateObject.getMinutes()) + ':' + seconds;

    return date + 'T' + time + 'Z';
}

const generateUniqueSignature = async (firstname, lastname) =>{
    const checkUnique = (sig) => {
        return !emailArray.includes(sig);
    }

    const genUniqueSig = (index = 0, shuffle = false, withNumbers = false) =>{
        const fLength = splitFirstname.length,
              lLength = splitLastname.length,
              namesInScope = (index > fLength) && (index + 1 > lLength);

        if(namesInScope && !shuffle) return genUniqueSig(0, true);
        else if (namesInScope && shuffle) return genUniqueSig(0, false, true);
        
        let newSig = firstname.substring(0, (index > fLength ? fLength + 1 : index + 1)) + 
            lastname.substring(0, (index + 1 > lLength ? lLength + 1 : index + 2));   
        
        if(withNumbers){
            const emailWithNumber = (sig, index) =>{
                const newSignature = sig.toLowerCase() + index.toString();
                return checkUnique(newSignature) ? newSignature : createUniqueEmail(sig, index + 1);
            }
            return emailWithNumber(newSig, 0);
        }

        if(!shuffle) return checkUnique(newSig.toLowerCase()) ? newSig : genUniqueSig(index + 1);
        let shuffleSig = newSig.split('');

        for(let i = 0; i < 10; i++){
            if(i > 0) shuffleSig = shuffleSig.split('');
            shuffleSig = shuffleArray(shuffleSig);
            shuffleSig = shuffleSig.toString().replace(/,/g, '');
            if(checkUnique(shuffleSig.toString().toLowerCase())) return shuffleSig;
        }
        return genUniqueSig(index + 1, true);
    }

    const splitFirstname = firstname.split(''),
          splitLastname = lastname.split('');
    
    let signature = await genUniqueSig().toString().toLowerCase();
    emailArray.push(signature);
    return signature;
}
