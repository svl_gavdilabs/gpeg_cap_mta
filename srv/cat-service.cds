using gp.service as gp from '../db/schema';

service EmployeeGenerator {

    @readonly entity EmployeeSet as projection on gp.Employee;
    
    action GenerateEmployees(info : gp.GenerationData) returns many EmployeeSet;

    action GenderCount() returns gp.GenderCount;

}