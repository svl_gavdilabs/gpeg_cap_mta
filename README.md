# README #

This version of the MTA is based on first creating a CAP backend and then using the provided folder structure from that template to generate a fullstack application. 

**List of Content**

- [README](#markdown-header-readme)
- [How it works](#markdown-header-how-it-works)
  - [Backend](#markdown-header-backend)
    - [API](#markdown-header-api)
      - [EmployeeSet](#markdown-header-employeeset)
        - [Employee Entity Structure](#markdown-header-employee-entity-structure)
      - [GenerateEmployees](#markdown-header-generateemployees)
        - [Info Object Parameters](#markdown-header-info-object-parameters)
        - [Custom Addresses Object](#markdown-header-custom-addresses-object)
    - [Unit Testing](#markdown-header-unit-testing)
    - [TO-DO](#markdown-header-to-do)
  - [Frontend](#markdown-header-frontend)
    - [Features](#markdown-header-features)
    - [TO-DO](#markdown-header-to-do-1)
  - [MTA](#markdown-header-mta)
- [How To Create Your Own MTA Project](#markdown-header-how-to-create-your-own-mta-project)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Initializing the Project](#markdown-header-initializing-the-project)
  - [Adding a UI](#markdown-header-adding-a-ui)
  - [Creating the MTA Descriptor](#markdown-header-creating-the-mta-descriptor)
  - [Deploying to Cloud Foundry](#markdown-header-deploying-to-cloud-foundry)
- [Resources](#markdown-header-resources)

****
# How it works 
The application makes use of the SAP CAP model and uses the original structure provided by the CDS init:

| Folder | Description                                                           |   |   |   |
|--------|-----------------------------------------------------------------------|---|---|---|
| /srv   | Service folder containing CAP service                                 |   |   |   |
| /db    | Database folder containing the entity schema and development database |   |   |   |
| /app   | The app folder contains the UI module for the application             | 

The project can easily be run both on your local machine and Cloud Foundry using the built-in scripts in the package.json file.

## Backend 
The backend for this runs on the Nodejs version of CDS. 

### API
Currently the API consists of the "*EmployeeSet*" and the action import "*GenerateEmployees*". 

#### EmployeeSet
The EmployeeSet is a *@readonly* entity in the current version of the API and can therefore only be reached with a GET request. 

All the generation and deletion is instead done through the *GenerateEmployees* action import. 
In the future I would like to make a couple of more action imports for more custom actions in relation to the EmployeeSet.

##### Employee Entity Structure
| Parameter    | Description                                   | Type     | Key |
|--------------|-----------------------------------------------|----------|-----|
| email        | Unique email signature for the employee       | String   | X   |
| male         | Determines the gender of the employee         | Boolean  | -   |
| firstName    | The firstname of the employee                 | String   | -   |
| lastName     | The lastname of the employee                  | String   | -   |
| dateOfBirth  | Date of birth                                 | DateTime | -   |
| streetname   | Streetname that the employee lives on         | String   | -   |
| streetnumber | The house number for the employee's residence | Integer  | -   |
| postalcode   | The postal code of the employee's city        | String   | -   |
| cellPhone    | The employee's phonenumber                    | String   | -   |
| country      | Country of residence                          | String   | -   |
| countryCode  | The corresponding country's country code      | String   | -   |
| pictureUrl   | Url for the image stored in the database      | String   | -   |

#### GenerateEmployees
The action import GenerateEmployees is the star of the show in this project. It can take in a variety of different parameters and then generate a set of randomly generated [employees](#markdown-header-employee-entity-structure) based on these parameters. The body of the POST request sent to this action is all contained inside an object called "*info*". 

##### Info Object Parameters
| Parameter        | Description                                                                                          | Relation  | Type        |
|------------------|------------------------------------------------------------------------------------------------------|-----------|-------------|
| domain           | The email domain you want to attach to the employee's email id                                       | MANDATORY | String      |
| amount           | The amount of employees you wish to generate in this call                                            | MANDATORY | Integer     |
| genPhone         | Determine whether or not the generator should generate random phonenumbers for the employees         | MANDATORY | Boolean     |
| genAddress       | Determine whether or not the generator should generate addresses for the employees                   | MANDATORY | Boolean     |
| minAge           | The lowest age that the employees can have                                                           | MANDATORY | Integer     |
| maxAge           | The highest age that the employees can have                                                          | MANDATORY | Integer     |
| femaleOnly       | Determine if you want the generator to only generate female entries                                  | MANDATORY | Boolean     |
| maleOnly         | Determine if you want the generator to only generate male entries                                    | MANDATORY | Boolean     |
| maleFirstnames   | Takes an array of new male names if you want to generate male names based on your own data           | OPTIONAL  | String[]    |
| femaleFirstnames | Takes an array of new male names if you want to generate female names based on your own data         | OPTIONAL  | String[]    |
| country          | The country initials that you want to use for your generated employees                               | OPTIONAL  | String      |
| countrycode      | Your own custom country code that you want to use for the generated employees                        | OPTIONAL  | String      |
| addresses        | Takes in an array of custom address objects (see [this section](#markdown-header-Custom-Addresses-Object)) to generate custom addresses for the employees | OPTIONAL  | Addresses[] |
| phoneDigits      | The amount of digits that you want the phonenumber for the employees to have. Default is 8.          | OPTIONAL  | Integer     |

##### Custom Addresses Object
| Parameter  | Description                                               | Relation  | Type   |
|------------|-----------------------------------------------------------|-----------|--------|
| streetname | The name of the street                                    | MANDATORY | String |
| postalcode | The postal code of the city that the street is located in | MANDATORY | String |

### Unit Testing
Currently the backend makes use of the Mocha and Chai packages to perform unit testing, alongside the Rewire package to allow for custom function testing. 

There are currently 20 tests set in place for the project, testing both the API and the custom functionality implemented in the backend. 

The project has a custom script setup in the top-level package.json file, so you can easily run the unit tests from the root folder using the command: ```npm run test```

All of the backend unit tests are also run before compiling the MTAD for the [MTA](#markdown-header-mta).

### TO-DO
The current version of the backend is not yet perfect, so here is a list of things that is yet to be done!

- Further custom actions for full control on the generation
- Fix generation bug when deployed on HANA database
- Concurrency

## Frontend
For this project I decided to make use of the scaffolding(folder structure) provided by the CDS init. 
However, instead of using the Fiori elements that you can generate using CAP, I created a freestyle UI5 application and setup custom routing between the two applications.

### Features
(to be written)

### TO-DO

- Allow for the user to upload JSON files as custom data
- Finalize the data viz view with graphs 
- Add data visualization selection for easier export filtering

## MTA
The project uses an MTA.yaml file in order to deploy to CF. 

The stack deploys into 3 different modules, a database service module running a Hana instance, a server module, and a UI module. 
For specifications on each module check out the mta.yaml file in the source.

****

# How To Create Your Own MTA Project 
In this section I'll go over how I went about creating the MTA project. This is a lengthy setup process, however I have setup a template repository that you can clone if you want an easier process and even a Yeoman generator if that's more your style! The template can be found [here](https://bitbucket.org/svl_gavdilabs/nodejs-cap-template/src/master/), and the Yeoman generator can be found [here](https://bitbucket.org/svl_gavdilabs/generator-cap-nodejs). 

## Prerequisites 
In order for you to create an MTA project, at least in Visual Studio Code, you'll need a couple of different things installed on your machine. 

- [@sap/cds-dk](https://cap.cloud.sap/docs/get-started/) *(The CDS development kit)*
- [Cloud Foundry CLI Tool](https://docs.cloudfoundry.org/cf-cli/install-go-cli.html)
- [Cloud Foundry MTA Plugin](https://github.com/cloudfoundry-incubator/multiapps-cli-plugin)
- [UI5 Tooling](https://github.com/SAP/ui5-tooling)
- [Node.js](https://nodejs.org/en/) 
- [Yeoman](https://yeoman.io/)
- [@sapui5/sapui5-templates ](https://sapui5.hana.ondemand.com/#/topic/a460a7348a6c431a8bd967ab9fb8d918.html)*(Templates to be used with Yeoman)*

I also recommend getting the CDS plugin for VS Code from the official source [here](https://tools.hana.ondemand.com/#cloud-vscodecds). 
You can also install from the extension tab inside VS Code. 

## Initializing the Project
First off we need to generate our backend project, which is going to be the root of the project. The initilization is pretty easy as we're just going to use the CDS CLI tool.

```cds init your-project-name```

Once you're done with the initialization of your project, remember to run ```npm install``` to make sure that all necessary packages have been added. 

After the npm process is done you're free to start modelling your service just as you want it. 
There is no magic behind this part of the development really, just create what you need.

By running the *cds init* command you get only the bare essentials for your project. A folder structure and the essential packages. This will not be enough to actually run the backend application... Because it isn't there yet.

In order to actually have a backend service we'll have to create it by setting up two files. A service is created by adding a "*.cds*" file to your *srv* folder. You can almost name it anything you want, as long as it contains *service* at the end. 

```yournewservicename-service.cds```

You now have an empty service, but we're going to need a schema of our data to make a communication between it and the database. So let's go to the db folder and create a schema.cds file. Inside this you create the data structures for the elements in your service. If you don't know exactly how, just take a look at how mine is setup. **REMEMBER TO SET A NAMESPACE FOR THE FILE!**

If we then go back to the "*~name~-service.cds*" file, we can then import this schema using the following line of code:

```using your-name-space as your-abbreviation from '../db/schema';```

And now you're pretty much ready to setup your API! But of course you want to have some custom logic, or at least the ability to create some, right? In order to do this we're going to setup another file. This file should be created in the same folder as your service.cds file, and have the same name, except the file-extension should be *.js*. 

If you then set the file up as follows:

```javascript
const cds = require ('@sap/cds');


module.exports = async function (srv) {
  /*Your custom logic goes here */
}
```
Then you should be ready to go! 

A quick heads up, I would recommend setting up an Sqlite3 persistent database in your project for local development and only use Hana once you're ready to deploy. 
In my project I decided to make the switch between the two fairly easy by setting up the following scripts:

**Sqlite:**
``` "db:sqlite": "cds deploy --to sqlite:db/gp-service.db" ```

**Hana:**
``` "db:hana": "cds deploy --to hana" ```

## Adding a UI
As you probably saw when you initialized the project, an app folder was created. 
We're now going to use this folder to host our UI module. 

Let's start by initializing our app by opening up the terminal and go down to our *app* sub-folder and executing the following command: ``` yo @sapui5/sapui5-templates ```

At the time of writing this, the sapui5-templates scaffolding only supports OData V4 in one of its templates, however if you want to make so that your backend can support the V2 scaffoldings you can add a proxy. 
If that tickles your fancy have a look at this [resource](https://blogs.sap.com/2020/06/30/how-to-add-odata-v2-adapter-proxy-to-your-cap-project/), which talks a bit about setting this proxy up.

Once you're done with the Yeoman scaffolding setup, perform an ```npm install``` inside the app folder and try and run the application using the UI5 tooling using the ```npm start``` command.

**MAKE SURE THAT YOU HAVE THE CDS PROCESS RUNNING IN A SEPARATE NODE OTHERWISE THIS WON'T WORK**

Now, in order for this to work on the Cloud Foundry platform we'll need to setup an approuter.

Inside the *app* folder, open the package.json file and setup the file to look like this:

```json
{
  "name": "frontend",
  "description": "",
  "scripts": {
    "build": "ui5 build",
    "serve": "ui5 serve -o index.html",
    "start": "node node_modules/@sap/approuter/approuter.js"
  },
  "dependencies": {
    "@sap/approuter": "^8.2.1"
  },
  "devDependencies": {
    "@ui5/cli": "^1.14.0",
    "ui5-middleware-simpleproxy": "^0.2.1"
  },
  "ui5": {
    "dependencies": [
      "ui5-middleware-simpleproxy"
    ]
  }
}

```

Once you've done this you'll need to setup the approuter file, named ```xs-app.json```. 
Create this file inside the app folder, along with another file called ```default-env.json```. 

Setup the ```xs-app.json``` file to look as follows:

```json
  
{
    "welcomeFile": "app/index.html",
    "authenticationMethod": "none",
    "logout": {
      "logoutEndpoint": "/do/logout"
    },
    "routes": [
      {
        "source": "/<your-api-base>/(.*)$",
        "target": "/<your-api-base>/$1",
        "destination": "srv_api"
      },
      {
        "source": "/v2/(.*)$",
        "target": "/v2/$1",
        "destination": "srv_api"
      },
      {
        "source": "^/app/(.*)$",
        "target": "$1",
        "localDir": "dist"
      },
      {
        "source": "^/appdebug/(.*)$",
        "target": "$1",
        "localDir": "webapp"
      }
    ]
  }
``` 

**Remember to swap out ```<your-api-base>``` with your actual api base point.** 

Now, let's setup the other file we created (```default-env.json```). 
This one is fairly simple, as we just need it to point to our destinations between the two apps.

```json
{
    "destinations":[
        {
            "name": "srv_api",
            "url": "http://localhost:4004"
        }
    ]
}
```

Once you've set that up you're more or less ready to test if the approuter is functional on your local machine. 
For you to do that you have to start up your cds service, and then open another terminal and navigate into the app folder and execute the following commands:

``` npm install && npm start```

the reason why we run the install first is because we added the approuter to our dependencies manually earlier.



## Creating the MTA Descriptor
So we now have both the backend service and the frontend UI ready for our application.
This means that we now move from the easy part and onto the harder part. 

In order for us to make the MTA descriptor and actually send it to Cloud Foundry, we're going to need some packages.
Open up the terminal and execute the following commands:

- ```npm i --save-dev mbt```
- ```npm i --save-dev cross-var```
- ```npm i --save-dev npm-run-all```

The only really necessary package is the first one (*mbt*), as the two other are more development quality of life packages, however it will make sense in a bit.

Open up your ``package.json`` file in the root folder, and let's set up some scripts that is going to make our life a bit easier.

```json
...
{
    "scripts": {
    "start": "npx cds run",
    "start:dev": "npx cds run & (cd app && npm run serve)",
    "build:mtad": "mbt build",
    "deploy:cf": "cross-var cf deploy mta_archives/gp-service_$npm_package_version.mtar",
    "deploy": "run-s build:mtad deploy:cf",
    "db:hana": "cds deploy --to hana",
    "db:sqlite": "cds deploy --to sqlite:db/gp-service.db"
  }
}
...
```
And now we're finally ready to actually setup the ``mta.yaml`` file.

Open up the terminal in the root folder and perform the command ``cds add mta``.
This will auto-generate an mta.yaml file for us so that we can easily just do some slight modifications to it without having to start from scratch.

Let's modify it to have the modules we need:

```yaml
## Generated mta.yaml based on template version 0.2.0
## appName = gp-service
## language=nodejs; multiTenant=false
## approuter=
_schema-version: '3.1'
ID: gp-service
version: 1.0.0
description: "A simple CAP project."
parameters:
  enable-parallel-deployments: true
  
  
build-parameters:
  before-all:
   - builder: custom
     commands:
      - npm install
      - npx cds build

modules:
 # --------------------- SERVER MODULE ------------------------
 - name: gp-srv
 # ------------------------------------------------------------
   type: nodejs
   path: gen/srv
   properties:
     EXIT: 1  # required by deploy.js task to terminate 
   requires:
    - name: gp-db-hdi-container
   provides:
    - name: srv-binding
      properties: 
        srv-url: ${default-url}
 
 # ----------------------- UI MODULE --------------------------
 - name: gp-frontend
 # ------------------------------------------------------------
   type: nodejs
   path: gen/app
   parameters:
    disk-quota: 256M
    memory: 256M
   build-parameters:
    builder: custom
    commands:
      - npm install
      - npm run build
   requires:
    - name: srv-binding
      group: destinations
      properties:
        name: srv_api
        url: '~{srv-url}'


resources:
  - name: gp-db-hdi-container
    parameters: 
      service: hanatrial
      service-plan: hdi-shared
    type: com.sap.xs.hdi-container
    properties: 
      hdi-container:name: ${service-name}
```

In here we setup the server module to require our new database service (which we're going to connect to in just a bit)
and create the database service in the resources section. 

We also setup our UI module with a custom builder, that way we are certain that the entry point for the app is going to be the approuter.

And now we should be more or less ready to deploy our app to Cloud Foundry!

## Deploying to Cloud Foundry

Okay, so here we go!

First we need to tell our CDS to use our cloud database, Hana, instead of our local development database, SQLite.
If you setup the scripts in the npm like I mentioned earlier it is as easy as running the command ``npm run db:hana`` 

This command will now generate the Hana database on Cloud Foundry. 
If this is the first time that you use Cloud Foundry inside this project it will prompt you to 
set the API endpoint of your CF account and your account login, just fill that out and you're ready to go. 

Now that our database is connected on CF we just need to build our MTAD file and deploy it to CF. 
All we have to do is just run the script for deployment ``npm run deploy`` and our application should now be on its way to the cloud!

Be aware that the CF environment can be a bit volatile so things might change over time. 
If you should encounter any problems trying to setup your app try and consult the [resources](#markdown-header-resources) or contact me on my email: *svl@gavdilabs.com* 

****

# Resources 

I couldn't have made this project without the work done by the people that came before me, so here is a few resources that I made use of to fully understand how to setup a project like this:

- [Adding UI to your CAP project](https://cap.cloud.sap/docs/get-started/in-a-nutshell#addingserving-uis)
- [SAP Cloud Samples](https://github.com/SAP-samples/cloud-cap-samples/tree/master/bookshop)
- [Approuter Documentation](https://github.com/gregorwolf/SAP-NPM-API-collection/tree/master/apis/approuter)
- [Deployment Descriptor Docs](https://github.com/cloudfoundry-incubator/multiapps-controller/wiki/Deployment-Descriptor)
- [MTA Deployment Docs](https://help.sap.com/viewer/4505d0bdaf4948449b7f7379d24d0f0d/2.0.03/en-US/33548a721e6548688605049792d55295.html)
- [UI5 Freestyle in CAP](https://blogs.sap.com/2020/07/08/ui5-freestyle-app-in-cap/)
- [Switching From SQLite to Hana DB](https://answers.sap.com/questions/12983343/switching-to-hana-db-and-deploying-the-business-se.html)
- [Deploying an App to Cloud Foundry](https://github.com/SAP-samples/cloud-cap-nodejs-codejam/tree/master/exercises/10)
- [Gregor Wolf MTA Example](https://github.com/gregorwolf/bookshop-nodejs)
- [Deploying SAP CAP Business Application to CF](https://developers.sap.com/tutorials/cap-service-deploy.html)
